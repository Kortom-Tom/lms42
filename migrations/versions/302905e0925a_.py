"""empty message

Revision ID: 302905e0925a
Revises: 
Create Date: 2020-08-18 09:41:29.218056

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '302905e0925a'
down_revision = None
branch_labels = None
depends_on = None

def upgrade():
    op.create_table('user',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('first_name', sa.String(), nullable=False),
        sa.Column('last_name', sa.String(), nullable=False),
        sa.Column('email', sa.String(), nullable=False),
        sa.Column('is_active', sa.Boolean(), nullable=False),
        sa.Column('is_teacher', sa.Boolean(), nullable=False),

        sa.Column('counselor_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['counselor_id'], ['user.id'], name=op.f('fk_user_counselor_id_user')),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_user')),
        sa.UniqueConstraint('email', name=op.f('uq_user_email'))
    )
    # Ids below 100 are reserved for special roles (at the moment just admin being #1):
    op.execute('SELECT setval(\'user_id_seq\', 100);')

    op.create_table('login_link',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('time', sa.DateTime(), nullable=False),
        sa.Column('secret', sa.String(), nullable=False),
        sa.Column('user_id', sa.Integer(), nullable=False),
        sa.Column('redirect_url', sa.String(), nullable=True),
        sa.ForeignKeyConstraint(['user_id'], ['user.id'], name=op.f('fk_login_link_user_id_user')),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_login_link'))
    )


def downgrade():
    op.drop_table('login_link')
    op.drop_table('user')
