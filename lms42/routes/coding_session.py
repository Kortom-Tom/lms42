from ..app import db, app, csrf
from ..models.coding_session import CodingSessionAttendant
from .curriculum import get_student
from flask_login import login_required, current_user
from flask import request
import sqlalchemy


@app.route('/coding_session/<node_id>/done', methods=['DELETE', 'POST'])
@login_required
@csrf.exempt
def coding_session_done(node_id):
    student = get_student()
    assert student
    if request.method == 'POST':
        csa = CodingSessionAttendant(node_id=node_id, user_id=student.id)
        db.session.add(csa)
    else:
        db.session.delete(CodingSessionAttendant.query.filter_by(node_id=node_id, user_id=student.id).one())
    db.session.commit()
    return "OK"
