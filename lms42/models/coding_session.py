from ..app import db
import datetime

class CodingSessionAttendant(db.Model):
    user_id = db.Column(db.Integer, primary_key=True)
    node_id = db.Column(db.String, primary_key=True)

    time = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False)
