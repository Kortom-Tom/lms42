from ..app import db, app
from .user import User
import datetime
import sqlalchemy

class DashboardQuery(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String, nullable=False)
    sql = db.Column(db.String, nullable=False)
    explanation = db.Column(db.String, nullable=False)
