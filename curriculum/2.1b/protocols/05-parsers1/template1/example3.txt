======================================== Tokens ========================================
'if' <keyword>
'(' <delimiter>
'1' <number>
')' <delimiter>
'var' <keyword>
'a' <identifier>
'=' <operator>
'0' <number>
';' <delimiter>
'else' <keyword>
'var' <keyword>
'a' <identifier>
'=' <operator>
'1' <number>
';' <delimiter>
'if' <keyword>
'(' <delimiter>
'a' <identifier>
')' <delimiter>
'{' <delimiter>
'var' <keyword>
'a' <identifier>
'=' <operator>
'3' <number>
';' <delimiter>
'}' <delimiter>
'else' <keyword>
'if' <keyword>
'(' <delimiter>
'1' <number>
')' <delimiter>
'{' <delimiter>
'var' <keyword>
'a' <identifier>
'=' <operator>
'4' <number>
';' <delimiter>
'}' <delimiter>
'' <eof>

======================================== AST ========================================
tree.Program(
    block=tree.Block(
        statements=[
            tree.IfStatement(
                condition=tree.Literal(value=1),
                yes=tree.Block(
                    statements=[
                        tree.Declaration(
                            identifier='a',
                            expression=tree.Literal(value=0)
                        )
                    ]
                ),
                no=tree.Block(
                    statements=[
                        tree.Declaration(
                            identifier='a',
                            expression=tree.Literal(value=1)
                        )
                    ]
                )
            ),
            tree.IfStatement(
                condition=tree.VariableReference(name='a'),
                yes=tree.Block(
                    statements=[
                        tree.Declaration(
                            identifier='a',
                            expression=tree.Literal(value=3)
                        )
                    ]
                ),
                no=tree.IfStatement(
                    condition=tree.Literal(value=1),
                    yes=tree.Block(
                        statements=[
                            tree.Declaration(
                                identifier='a',
                                expression=tree.Literal(value=4)
                            )
                        ]
                    ),
                    no=None
                )
            )
        ]
    )
)

======================================== Running program ========================================
