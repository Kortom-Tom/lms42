======================================== Tokens ========================================
'print' <identifier>
'(' <delimiter>
'21' <number>
'*' <operator>
'2' <number>
')' <delimiter>
';' <delimiter>
'print' <identifier>
'(' <delimiter>
'25' <number>
'+' <operator>
'25' <number>
'-' <operator>
'8' <number>
')' <delimiter>
';' <delimiter>
'print' <identifier>
'(' <delimiter>
'2' <number>
'+' <operator>
'(' <delimiter>
'2' <number>
'*' <operator>
'20' <number>
')' <delimiter>
')' <delimiter>
';' <delimiter>
'var' <keyword>
'brand' <identifier>
'=' <operator>
'input_str' <identifier>
'(' <delimiter>
'"Favorite car brand? "' <string>
')' <delimiter>
';' <delimiter>
'if' <keyword>
'(' <delimiter>
'brand' <identifier>
'==' <operator>
'"lambo"' <string>
')' <delimiter>
'{' <delimiter>
'var' <keyword>
'brand' <identifier>
'=' <operator>
'"lamborghini"' <string>
';' <delimiter>
'}' <delimiter>
'print' <identifier>
'(' <delimiter>
'"Indeed! "' <string>
'+' <operator>
'brand' <identifier>
'+' <operator>
'"s are awesome cars!"' <string>
')' <delimiter>
';' <delimiter>
'var' <keyword>
'favorite' <identifier>
'=' <operator>
'input_num' <identifier>
'(' <delimiter>
'"Favorite number? "' <string>
')' <delimiter>
';' <delimiter>
'if' <keyword>
'(' <delimiter>
'favorite' <identifier>
'<' <operator>
'10' <number>
')' <delimiter>
'{' <delimiter>
'print' <identifier>
'(' <delimiter>
'"That\'s not much. Let\'s multiply!"' <string>
')' <delimiter>
';' <delimiter>
'var' <keyword>
'favorite' <identifier>
'=' <operator>
'favorite' <identifier>
'*' <operator>
'100' <number>
';' <delimiter>
'}' <delimiter>
'print' <identifier>
'(' <delimiter>
'"The awesomest number in the universe is"' <string>
',' <delimiter>
'favorite' <identifier>
',' <delimiter>
'"!"' <string>
')' <delimiter>
';' <delimiter>
'' <eof>

======================================== AST ========================================
tree.Program(
    block=tree.Block(
        statements=[
            tree.FunctionCall(
                name='print',
                arguments=[
                    tree.BinaryOperator(
                        left=tree.Literal(value=21),
                        operator='*',
                        right=tree.Literal(value=2)
                    )
                ]
            ),
            tree.FunctionCall(
                name='print',
                arguments=[
                    tree.BinaryOperator(
                        left=tree.BinaryOperator(
                            left=tree.Literal(value=25),
                            operator='+',
                            right=tree.Literal(value=25)
                        ),
                        operator='-',
                        right=tree.Literal(value=8)
                    )
                ]
            ),
            tree.FunctionCall(
                name='print',
                arguments=[
                    tree.BinaryOperator(
                        left=tree.Literal(value=2),
                        operator='+',
                        right=tree.BinaryOperator(
                            left=tree.Literal(value=2),
                            operator='*',
                            right=tree.Literal(value=20)
                        )
                    )
                ]
            ),
            tree.Declaration(
                identifier='brand',
                expression=tree.FunctionCall(
                    name='input_str',
                    arguments=[tree.Literal(value='Favorite car brand? ')]
                )
            ),
            tree.IfStatement(
                condition=tree.BinaryOperator(
                    left=tree.VariableReference(name='brand'),
                    operator='==',
                    right=tree.Literal(value='lambo')
                ),
                yes=tree.Block(
                    statements=[
                        tree.Declaration(
                            identifier='brand',
                            expression=tree.Literal(value='lamborghini')
                        )
                    ]
                ),
                no=None
            ),
            tree.FunctionCall(
                name='print',
                arguments=[
                    tree.BinaryOperator(
                        left=tree.BinaryOperator(
                            left=tree.Literal(value='Indeed! '),
                            operator='+',
                            right=tree.VariableReference(name='brand')
                        ),
                        operator='+',
                        right=tree.Literal(value='s are awesome cars!')
                    )
                ]
            ),
            tree.Declaration(
                identifier='favorite',
                expression=tree.FunctionCall(
                    name='input_num',
                    arguments=[tree.Literal(value='Favorite number? ')]
                )
            ),
            tree.IfStatement(
                condition=tree.BinaryOperator(
                    left=tree.VariableReference(name='favorite'),
                    operator='<',
                    right=tree.Literal(value=10)
                ),
                yes=tree.Block(
                    statements=[
                        tree.FunctionCall(
                            name='print',
                            arguments=[
                                tree.Literal(
                                    value="That's not much. Let's multiply!"
                                )
                            ]
                        ),
                        tree.Declaration(
                            identifier='favorite',
                            expression=tree.BinaryOperator(
                                left=tree.VariableReference(name='favorite'),
                                operator='*',
                                right=tree.Literal(value=100)
                            )
                        )
                    ]
                ),
                no=None
            ),
            tree.FunctionCall(
                name='print',
                arguments=[
                    tree.Literal(
                        value='The awesomest number in the universe is'
                    ),
                    tree.VariableReference(name='favorite'),
                    tree.Literal(value='!')
                ]
            )
        ]
    )
)

======================================== Running program ========================================
42
42
42
Favorite car brand? Indeed! lamborghinis are awesome cars!
Favorite number? The awesomest number in the universe is 42 !
