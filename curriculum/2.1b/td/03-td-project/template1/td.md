# Helicopter view

```plantuml
@startuml C4_Elements
!include <C4/C4_Context>

LAYOUT_WITH_LEGEND()

@enduml
```



# Basic architecture

TODO: Copy from *Helicopter view* and modify it. Don't forget to update the `!include` or you won't be able to create `Container`s.

## iOS frontend (partial example)

| Alternative | Rating | What                              |
| ----------- | ------ | --------------------------------- |
| Native      | +++    | Best user experience.             |
|             | ---    | Most work.                        |
|             | ---    | No code sharing with iOS.         |
|             | --     | All developers need Macbooks.     |
| Flutter     | ++     | Most code can be shared with iOS. |
| ???????     | +++    | ????????                          |
|             | +      | ????????                          |
| ????????    | +++    | ????????                          |
|             | +++    | ????????                          |
|             | ---    | ????????                          |
|             | --     | ????????                          |

Choice: ????????

Motivation:????????, ????????, ????????, and we already have experience with a lot of the technology. We should be able to deliver quickly!

## Foobar backend (example)

TODO: Alternative comparison, choice and motivation like above.

## Payment service (example)

TODO: Alternative comparison, choice and motivation like above.

## Etc...

TODO: Alternative comparison, choice and motivation like above.



# Scalable architecture

TODO: Copy your *Basic architecture* model here and modify it.

TODO: Explanations.



# Reliability

TODO: Explanation for each container.



# Security

TODO: Explanation for each relationship.



# Backend components

TODO: C4 level 3 model.

TODO: Pros/cons.

TODO: Protocols and discoverability.
