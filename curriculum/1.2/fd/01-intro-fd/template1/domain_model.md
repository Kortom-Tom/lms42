# Domain model

```plantuml
' Configuration stuff (you should not change this)
' hide the spot
hide circle
' avoid problems with angled crows feet
skinparam linetype ortho


' Add the entities here
entity "Entity01" as e01 {
  *name : text
  description : text
}

entity "Entity02" as e02 {
  *price : decimal
  other_details : text
}

' Add notes here
note left of e01
  This is a multi line note:
  With <i>italcs</i> and <b>bold</b> markup
end note


' Add the relationships here
e01 ||..o{ e02 : owned

```