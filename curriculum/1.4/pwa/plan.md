1. JavaScript and the DOM ==> Enhance a web page using client-side-only JavaScript. (Login form)
2. Node.js, NPM and async/await ==> Do something non-webby, using some NPM libs including SQLite, with and without async/await. (ToDo-manager with telnet-interface)
3. Express, REST ==> Create a REST service with an SQLite database. (ToDo-manager)
4. SPA ==> Write a vanilla JS SPA for the above API. (ToDo-manager)
5. Svelte ==> Rewrite your SPA in Svelte and extend it. (ToDo-manager) 
7. WebSockets & PWAs. (Chat app)
8. E2E testing with Cypress. (Stock trader)
9. Exam project (3 days) ==> Come up with an idea yourself (before the exam starts)! Must include all of the above, except vanilla JS.

